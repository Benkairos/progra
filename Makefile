prog.exe : obj/main.o obj/cliente.o obj/cheque.o obj/efectivo.o obj/tarjeta.o obj/correo.o obj/entrega.o obj/productos.o obj/pedido.o obj/productoDetalle.o obj/criticas.o
	g++ -o prog.exe obj/main.o obj/cliente.o obj/cheque.o obj/efectivo.o obj/tarjeta.o obj/correo.o obj/entrega.o obj/productos.o obj/pedido.o obj/productoDetalle.o obj/criticas.o

obj/main.o : main.cc obj/cliente.o obj/cheque.o obj/efectivo.o obj/tarjeta.o obj/correo.o obj/entrega.o obj/productos.o obj/pedido.o obj/productoDetalle.o obj/criticas.o
	g++ main.cc -c -o obj/main.o 

obj/correo.o : Clases/correo/correo.h Clases/correo/correo.cc
	g++ Clases/correo/correo.cc -c -o obj/correo.o

obj/entrega.o : Clases/entrega/entrega.h Clases/entrega/entrega.cc
	g++ Clases/entrega/entrega.cc -c -o obj/entrega.o
	
obj/cliente.o : Clases/cliente/cliente.h Clases/cliente/cliente.cc
	g++ Clases/cliente/cliente.cc -c -o obj/cliente.o

obj/cheque.o : Clases/pago/cheque/cheque.h Clases/pago/cheque/cheque.cc
	g++ Clases/pago/cheque/cheque.cc -c -o obj/cheque.o

obj/efectivo.o : Clases/pago/efectivo/efectivo.h Clases/pago/efectivo/efectivo.cc
	g++ Clases/pago/efectivo/efectivo.cc -c -o obj/efectivo.o

obj/tarjeta.o : Clases/pago/tarjeta/tarjeta.h Clases/pago/tarjeta/tarjeta.cc
	g++ Clases/pago/tarjeta/tarjeta.cc -c -o obj/tarjeta.o	

obj/productos.o : Clases/productos/productos.h Clases/productos/productos.cc
	g++ Clases/productos/productos.cc -c -o obj/productos.o

obj/pedido.o : Clases/pedido/pedido.h Clases/pedido/pedido.cc
	g++ Clases/pedido/pedido.cc -c -o obj/pedido.o

obj/criticas.o : Clases/criticas/criticas.h Clases/criticas/criticas.cc
	g++ Clases/criticas/criticas.cc -c -o obj/criticas.o

obj/productoDetalle.o : Clases/productoDetalle/productoDetalle.h Clases/productoDetalle/productoDetalle.cc
	g++ Clases/productoDetalle/productoDetalle.cc -c -o obj/productoDetalle.o

all:
	rm obj/*
	make
