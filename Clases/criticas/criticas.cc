#include "criticas.h"

Criticas::Criticas(string pedidoId)
{
    this->pedidoAsociadoID = pedidoId;
}

Criticas::~Criticas() {}

string Criticas::getPedidoAsociado()
{
    return this->pedidoAsociadoID;
}

vector<string> Criticas::getCriticas()
{
    return this->vectorCriticas;
}

void Criticas::pushCriticas(string critica)
{
    this->vectorCriticas.push_back(critica);
}

void Criticas::setPedidoAsociado(string pedidoID)
{
    this->pedidoAsociadoID = pedidoID;
}
