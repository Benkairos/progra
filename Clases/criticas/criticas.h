#ifndef CRITICAS_H
#define CRITICAS_H

#include <vector>
#include <iostream>
#include <string>

using namespace std;
class Criticas
{
private:
    vector<string> vectorCriticas;
    string pedidoAsociadoID;
public:
    Criticas(string pedidoID);
    ~Criticas();
    string getPedidoAsociado();
    void setPedidoAsociado(string pedidoID);

    void pushCriticas(string critica);
    vector<string> getCriticas();
};




#endif // CRITICAS_H
