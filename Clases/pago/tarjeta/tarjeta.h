#ifndef TARJETA_H
#define TARJETA_H


#include <iostream>
#include <string>

using namespace std;

typedef enum {Credito, Debito, Visa, Mastercard } TipoTarjeta;
class Tarjeta
{
private:
    TipoTarjeta tipo;
    string caducidad;
    int numTarjeta;
    int numCuotas;
    
public:
    Tarjeta(TipoTarjeta tipo, string caducidad, int numTarjeta, int numCuotas);
    ~Tarjeta() {};

    string getCaducidad();
    void setCaducidad( string caducidad );

    int getNumTarjeta();
    void setNumTarjeta( int numTarjeta );

    int getNumCuotas();
    void setNumCuotas( int numCuotas );

    TipoTarjeta getTipo();
    void setTipo( TipoTarjeta tipo );
};

#endif
