#include "tarjeta.h"

Tarjeta::Tarjeta(TipoTarjeta tipo, string caducidad, int numTarjeta, int numCuotas) {
    this->tipo = tipo;
    this->caducidad = caducidad;
    this->numTarjeta = numTarjeta;
    this->numCuotas = numCuotas;
}

int Tarjeta::getNumTarjeta() 
{
    return numTarjeta;
}

void Tarjeta::setNumTarjeta(int numTarjeta)
{
    this->numTarjeta = numTarjeta;
}

int Tarjeta::getNumCuotas() 
{
    return numCuotas;
}

void Tarjeta::setNumCuotas(int numCuotas)
{
    this->numCuotas = numCuotas;
}

string Tarjeta::getCaducidad() 
{
    return caducidad;
}

void Tarjeta::setCaducidad(string caducidad)
{
    this->caducidad = caducidad;
}

TipoTarjeta Tarjeta::getTipo() 
{
    return tipo;
}

void Tarjeta::setTipo(TipoTarjeta tipo)
{
    this->tipo = tipo;
}
