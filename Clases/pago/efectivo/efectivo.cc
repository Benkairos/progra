#include "efectivo.h"

Efectivo::Efectivo(int montoMonedas, int montoBilletes)
{
    this->montoBilletes = montoBilletes;
    this->montoMonedas = montoMonedas;
}

int Efectivo::getmontoMonedas() {
    return this->montoMonedas;
}

int Efectivo::getmontoBilletes() {
    return this->montoBilletes;
}

void Efectivo::setmontoMonedas( int montoMonedas ) {
    this->montoMonedas = montoMonedas;
}

void Efectivo::setmontoBilletes( int montoBilletes ) {
    this->montoBilletes = montoBilletes;
}
