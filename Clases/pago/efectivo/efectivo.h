#ifndef EFECTIVO_H
#define EFECTIVO_H


#include <iostream>
#include <string>

using namespace std;

class Efectivo
{
private:
    int montoMonedas;
    int montoBilletes;
public:
    Efectivo(int montoMonedas, int montoBilletes);
    ~Efectivo() {};

    int getmontoMonedas();
    void setmontoMonedas( int montoMonedas );

    int getmontoBilletes();
    void setmontoBilletes( int montoBilletes );

};

#endif // EFECTIVO_H
