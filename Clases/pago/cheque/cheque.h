#ifndef CHEQUE_H
#define CHEQUE_H


#include <iostream>
#include <string>

using namespace std;

class Cheque
{
private:
    // TipoCheque tipo;
    string nombre;
    string banco;
    string fecha;
public:
    Cheque(string nombre, string banco, string fecha);
    ~Cheque() {};
    string getnombre();
    void setnombre( string nombre );

    string getbanco();
    void setbanco( string banco );

    string getfecha();
    void setfecha( string fecha );
};

#endif // CHEQUE_H
