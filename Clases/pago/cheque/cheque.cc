#include "cheque.h"

Cheque::Cheque(string nombre, string banco, string fecha) {
    this->nombre = nombre;
    this->banco = banco;
    this->fecha = fecha;
}

string Cheque::getnombre() {
    return this->nombre;
}

string Cheque::getbanco() {
    return this->banco;
}

string Cheque::getfecha() {
    return this->fecha;
}


void Cheque::setnombre( string nombre ) {
    this->nombre = nombre;
}

void Cheque::setbanco( string banco ) {
    this->banco = banco;
}

void Cheque::setfecha( string fecha ) {
    this->fecha = fecha;
}
