#ifndef PRODUCTODETALLE_H
#define PRODUCTODETALLE_H

#include "../productos/productos.h"
class ProductoDetalle
{
private:
    Productos producto;
    int cantidad;
    float total;
public:
    ProductoDetalle(Productos, int, float);
    ~ProductoDetalle() {};

    void setCantidad(int);
    int getCantidad();

    void setTotal(float);
    float getTotal();

    Productos getProducto();
};

#endif // PRODUCTODETALLE_H
