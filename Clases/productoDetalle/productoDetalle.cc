#include "productoDetalle.h"

ProductoDetalle::ProductoDetalle( Productos producto, int cantidad, float total ) {
    this->producto = producto;
    this->cantidad = cantidad;
    this->total = total;
}

void ProductoDetalle::setCantidad( int cant ) {
    this->cantidad = cant;
}
int ProductoDetalle::getCantidad() {
    return this->cantidad;
}
void ProductoDetalle::setTotal( float total) {
    this->total = total;
}
float ProductoDetalle::getTotal() {
    return this->total;
}

Productos ProductoDetalle::getProducto() {
    return this->producto;
}