#include "pedido.h"

Pedido::Pedido(Cliente cliente, EstadoPedido estadoPedido, Correo correo, string pedidoId, string fechaFormaliza , vector<ProductoDetalle> productos, float totalPedido)
{
    this->cliente = cliente;
    this->estadoPedido = estadoPedido;
    this->correo = correo;
    this->pedidoId = pedidoId;
    this->fechaFormaliza = fechaFormaliza;
    this->pedidoDetalle = productos;
    this->totalPedido = totalPedido;
    // this->pago = nullptr;
}

Cliente Pedido::getCliente()
{
    return this->cliente;
}
void Pedido::setCliente(Cliente cliente)
{
    this->cliente = cliente;
}
EstadoPedido Pedido::getEstadoPedido()
{
    return this->estadoPedido;
}
void Pedido::setEstadoPedido(EstadoPedido estadoPedido)
{
    this->estadoPedido = estadoPedido;
}
Correo Pedido::getCorreoPostal()
{
    return this->correo;
}
void Pedido::setCorreoPostal(Correo correoPostal)
{
    this->correo = correoPostal;
}
string Pedido::getPedidoId()
{
    return this->pedidoId;
}
void Pedido::setPedidoId(string pedidoId)
{
    this->pedidoId = pedidoId;
}
string Pedido::getFechaFormaliza()
{
    return this->fechaFormaliza;
}
void Pedido::setFechaFormaliza(string fechaFormaliza)
{
    this->fechaFormaliza = fechaFormaliza;
}

vector<ProductoDetalle> Pedido::getPedidoDetalle() {
    return this->pedidoDetalle;
}