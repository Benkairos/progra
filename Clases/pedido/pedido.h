#ifndef PEDIDO_H
#define PEDIDO_H

#include <iostream>
#include <string>
#include <vector>
#include "../cliente/cliente.h"
#include "../correo/correo.h"
#include "../productoDetalle/productoDetalle.h"

using namespace std;

typedef enum {pendiente, pagado, procesando ,enviado, entregado} EstadoPedido;

class Pedido
{
private:

    Cliente cliente;
    Correo correo;
    EstadoPedido estadoPedido;
    string pedidoId;
    string fechaFormaliza;
    vector<ProductoDetalle> pedidoDetalle;
    float totalPedido;

public:
    Pedido() {};
    Pedido(Cliente cliente, EstadoPedido estadoPedido, Correo correo, string pedidoId, string fechaFormaliza, vector<ProductoDetalle> productos, float totalPedido);
    ~Pedido() {};

    Cliente getCliente();
    void setCliente( Cliente cliente );

    EstadoPedido getEstadoPedido();
    void setEstadoPedido( EstadoPedido estadoPedido );

    Correo getCorreoPostal();
    void setCorreoPostal( Correo );

    string getPedidoId();
    void setPedidoId( string );

    string getFechaFormaliza();
    void setFechaFormaliza( string );

    vector<ProductoDetalle> getPedidoDetalle();

    void setTotalPedido(float);
    float getTotalPedido();
};

#endif // PEDIDO_H


