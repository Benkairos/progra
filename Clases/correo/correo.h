#ifndef CORREO_H
#define CORREO_H

#include <iostream>
#include <string>

using namespace std;

class Correo
{
private:
    string nombreEmpresa;
    int celular;
    string representante;
    int telefono;
    string email;
    string direccion;

public:
    Correo() {};
    Correo(string nombreEmpresa, int celular, string representante, int telefono, string email, string direccion );
    ~Correo() {};

    int getCelular();
    void setCelular( int celular );

    string getRepresentante();
    void setRepresentante( string representante );

    int getTelefono();
    void setTelefono( int telefono );

    string getEmail();
    void setEmail( string email );

    string getDireccion();
    void setDireccion( string direccion );
    
    string getNombreEmpresa();
    void setNombreEmpresa( string nombreEmpresa );
};


#endif // CORREO_H