#include "correo.h"

Correo::Correo(string nombreEmpresa, int celular, string representante, int telefono, string email, string direccion)
{
    this->nombreEmpresa = nombreEmpresa;
    this->celular = celular;
    this->representante = representante;
    this->telefono = telefono;
    this->email = email;
    this->direccion = direccion;
}

int Correo::getCelular()
{
    return this->celular;
}

string Correo::getRepresentante()
{
    return this->representante;
}

int Correo::getTelefono()
{
    return this->telefono;
}

string Correo::getEmail()
{
    return this->email;
}

string Correo::getDireccion()
{
    return this->direccion;
}

void Correo::setCelular(int celular)
{
    this->celular = celular;
}

void Correo::setRepresentante(string representante)
{
    this->representante = representante;
}

void Correo::setTelefono(int telefono)
{
    this->telefono = telefono;
}

void Correo::setEmail(string email)
{
    this->email = email;
}

void Correo::setDireccion(string direccion)
{
    this->direccion = direccion;
}

string Correo::getNombreEmpresa() {
    return this->nombreEmpresa;
}

void Correo::setNombreEmpresa(string nombreEmpresa) {
    this->nombreEmpresa = nombreEmpresa;
}