#ifndef ENTREGA_H
#define ENTREGA_H

#include <iostream>
#include <string>

using namespace std;

class Entrega
{
private:
    string nombreReceptor;
    string rutReceptor;
    string nombreEmpleado;
    string fecha;

public:
    Entrega(string nombreR, string rutR, string nombreE, string fecha);
    ~Entrega() {};

    string getNombreReceptor();
    void setNombreReceptor( string nombreReceptor );

    string getRutReceptor();
    void setRutReceptor( string rutReceptor );

    string getNombreEmpleado();
    void setNombreEmpleado( string nombreEmpleado );

    string getFecha();
    void setFecha( string fecha );
    

};

#endif // ENTREGA_H