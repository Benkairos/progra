#include "entrega.h"

Entrega::Entrega(string nombreR, string rutR, string nombreE, string fecha)
{
    this->nombreReceptor = nombreR;
    this->rutReceptor = rutR;
    this->nombreEmpleado = nombreE;
    this->fecha = fecha;
}

string Entrega::getNombreReceptor()
{
    return this->nombreReceptor;
}

string Entrega::getRutReceptor()
{
    return this->rutReceptor;
}

string Entrega::getNombreEmpleado()
{
    return this->nombreEmpleado;
}


string Entrega::getFecha()
{
    return this->fecha;
}

void Entrega::setNombreReceptor(string nombreR)
{
    this->nombreReceptor = nombreR;
}

void Entrega::setRutReceptor(string rutR)
{
    this->rutReceptor = rutR;
}

void Entrega::setNombreEmpleado(string nombreE)
{
    this->nombreEmpleado = nombreE;
}

void Entrega::setFecha(string fecha)
{
    this->fecha = fecha;
}