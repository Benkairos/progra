#ifndef PRODUCTO_H
#define PRODUCTO_H

#include <iostream>
#include <string>

using namespace std;

class Productos
{
private:
    string nombreProducto;
    int precio;
    float impuesto;

public:
    Productos() {};
    Productos(string nombre, int precio, float impuesto);
    ~Productos() {};

    string getNombreProducto();
    void setNombreProducto( string nombre );

    int getPrecio();
    void setPrecio( int precio );

    float getImpuesto();
    void setImpuesto( float impuesto );
};

#endif // PRODUCTO_H
