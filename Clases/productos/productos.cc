#include "productos.h"

Productos::Productos(string nombre, int precio, float impuesto) {
    this->nombreProducto = nombre;
    this->precio = precio;
    this->impuesto = impuesto;
}

string Productos::getNombreProducto() {
    return this->nombreProducto;
}

int Productos::getPrecio() {
    return this->precio;
}

float Productos::getImpuesto() {
    return this->impuesto;
}


void Productos::setNombreProducto( string nombre ) {
    this->nombreProducto = nombre;
}

void Productos::setPrecio( int precio ) {
    this->precio = precio;
}

void Productos::setImpuesto( float impuesto ) {
    this->impuesto = impuesto;
}
