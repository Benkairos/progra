#include "cliente.h"

// Cliente::~Cliente() {}

Cliente::Cliente(string nombre, string rut, string clienteId) {
    this->nombre = nombre;
    this->rut = rut;
    this->clienteId = clienteId;
}

string Cliente::getNombre() {
    return this->nombre;
}

string Cliente::getRut() {
    return this->rut;
}

string Cliente::getClienteId() {
    return this->clienteId;
}


void Cliente::setNombre( string nombre ) {
    this->nombre = nombre;
}

void Cliente::setRut( string rut ) {
    this->rut = rut;
}

void Cliente::setClienteId( string clienteId ) {
    this->clienteId = clienteId;
}