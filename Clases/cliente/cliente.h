#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
#include <string>

using namespace std;

class Cliente
{
private:
    string nombre;
    string rut;
    string clienteId;

public:
    Cliente() {};
    Cliente(string nombre, string rut, string clienteId);
    ~Cliente() {};
    
    string getNombre();
    void setNombre( string nombre );

    string getRut();
    void setRut( string rut );

    string getClienteId();
    void setClienteId( string clienteId );

};

#endif // CLIENTE_H