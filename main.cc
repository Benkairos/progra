#include "module.h"
#include "menu.h"
#include "menuP.h"
#include <vector>

using namespace std;

vector<Cliente> clientes;
vector<Correo> correosPostales;
vector<Pedido> pedidos;
vector<Productos> productos;
bool flag = true;

int main(int argc, char const *argv[])
{
    clientes.push_back(Cliente("Jorge Aburto", "20.333.969-0", "123456789"));
    clientes.push_back(Cliente("Pedro Henriquez", "9.567.398-2", "123405507"));
    clientes.push_back(Cliente("Andrés Flores", "15.912.487-7", "1239874563"));

    correosPostales.push_back(Correo("Correos de chile",2123112, "Juanito", 32131213, "feo.com", "mi casita"));
    correosPostales.push_back(Correo("Starken",53424234, "Pedrito", 1231321, "horrible.com", "casita de mi awelita"));

    productos.push_back(Productos("Morrón", 800, 952));
    productos.push_back(Productos("Lechuga", 700, 833));
    productos.push_back(Productos("Manzana", 1200, 1428));
    productos.push_back(Productos("Durazno", 1500, 1785));

    //pedidos.push_back(Pedido(clientes[0], entregado, correosPostales[0], "feo", "1/12/2020", vector<ProductoDetalle>().push_back(ProductoDetalle(productos[0], 3, 15000)), 45000));
    while (flag)
    {
        int opcionSeleccionada = menu();
        if (opcionSeleccionada == 1)
            clientes.push_back(ingresarClienteMenu());
        if (opcionSeleccionada == 2)
            productos.push_back(ingresarProductoMenu());
        if (opcionSeleccionada == 3)
            pedidos.push_back(menuPedido(clientes, correosPostales, productos));
        if (opcionSeleccionada == 4)
            correosPostales.push_back(ingresarCorreoMenu());
        if (opcionSeleccionada == 5) {
            system(clear);
            listarPedidoNentregado(pedidos);
            pause();
        }
        if (opcionSeleccionada == 6) {
            system(clear);
            listarPedidoEntregado(pedidos);
            pause();
        }
        if (opcionSeleccionada == 7)
        {
            system(clear);
            listarClientes(clientes);
            pause();
        }
        if (opcionSeleccionada == 8)
        {
            system(clear);
            listarTodosLosPedidos(pedidos);
            pause();
        }
        if (opcionSeleccionada == 9)
        {
            system(clear);
            listarCorreos(correosPostales);
            pause();
        }
        if (opcionSeleccionada == 0)
        {
            system(clear);
            cout << "Ha salido del programa";
            break;
        }
    }
    return 0;
}