#ifndef MENU_H
#define MENU_H



#ifdef _WIN64
    char clear[6] = "clear";
#endif
#ifdef __APPLE__
    char clear[6] = "clear";
#endif
// Agregue su sistema operativo aquí
// #ifdef __APPLE__
//     char clear[6] = "clear";
// #endif





#include <iostream>
#include <vector>
#include <ctime>
#include <iomanip>
#include "module.h"
#include "string"

Cliente ingresarClienteMenu()
{
    char confirmar;
    string nombre;
    string rut;
    system(clear);
    cout << "\n\nIngresar un Cliente\n"
         << "\n- Nombre del cliente: "
         << "\n- Rut del cliente: "
         << "\n\nIngrese un nombre: ";
    getline(cin >> ws, nombre);
    system(clear);
    cout << "\n\nIngresar un Cliente\n"
         << "\n- Nombre del cliente: " << nombre << "\n- Rut del cliente: "
         << "\n\nIngrese un rut: ";
    getline(cin >> ws, rut);
    system(clear);
    cout << "\n\nIngresar un Cliente\n"
         << "\n- Nombre del cliente: " << nombre << "\n- Rut del cliente: " << rut << "\n\nDesea agregar al cliente (y/n) ? ";
    cin >> confirmar;
    if (confirmar == 'y')
    {
        time_t clienteId;
        clienteId = time(NULL);
        cout << "\n Ha agregado correctamente el cliente";
        cout << "\n Presione enter para continuar";
        cin.clear();
        cin.ignore();
        cin.get();
        return Cliente(nombre, rut, to_string(clienteId));
    }
}

Productos ingresarProductoMenu()
{
    char confirmar;
    string nombreProducto;
    int precio;
    float impuesto;
    system(clear);
    cout << "\n\nIngresar un Producto\n"
         << "\n- Nombre del producto: "
         << "\n- Precio del producto: "
         << "\n- Precio con iva: "
         << "\n\nIngrese el nombre del producto: ";
    getline(cin >> ws, nombreProducto);
    system(clear);
    cout << "\n\nIngresar un Producto\n"
         << "\n- Nombre del producto: " << nombreProducto << "\n- Precio del producto: "
         << "\n- Precio con iva: "
         << "\n\nIngrese el precio del producto: ";
    cin >> precio;
    impuesto = precio + precio * 0.19;
    system(clear);
    cout << "\n\nIngresar un Producto\n"
         << "\n- Nombre del producto: " << nombreProducto << "\n- Precio del producto: " << precio << "\n- Precio con iva: " << impuesto << "\n\nDesea agregar el producto (y/n) ? ";
    cin >> confirmar;
    if (confirmar == 'y')
    {
        cout << "\n Ha agregado correctamente el producto";
        cout << "\n Presione enter para continuar";
        cin.clear();
        cin.ignore();
        cin.get();
        return Productos(nombreProducto, precio, impuesto);
    }
}

Correo ingresarCorreoMenu()
{
    char confirmar;
    int celular;
    string nombreR;
    int telefono;
    string email;
    string direccion;
    string nombreEmpresa;
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: "
         << "\n- Numero de celular: "
         << "\n- Nombre de representante: "
         << "\n- Numero de telefono: "
         << "\n- Email: "
         << "\n- Direccion: "
         << "\n\nIngrese un celular (solo 8 numeros): +56 9";
    cin >> nombreEmpresa;
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: " << nombreEmpresa
         << "\n- Numero de celular: "
         << "\n- Nombre de representante: "
         << "\n- Numero de telefono: "
         << "\n- Email: "
         << "\n- Direccion: "
         << "\n\nIngrese un celular (solo 8 numeros): +56 9";
    cin >> celular;
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: " << nombreEmpresa
         << "\n- Numero de celular: " << celular << "\n- Nombre de representante: "
         << "\n- Numero de telefono: "
         << "\n- Email: "
         << "\n- Direccion: "
         << "\n\nIngrese un nombre de representante: ";
    getline(cin >> ws, nombreR);
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: " << nombreEmpresa
         << "\n- Numero de celular: " << celular << "\n- Nombre de representante: " << nombreR << "\n- Numero de telefono: "
         << "\n- Email: "
         << "\n- Direccion: "
         << "\n\nIngrese un telefono: ";
    cin >> telefono;
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: " << nombreEmpresa
         << "\n- Numero de celular: " << celular << "\n- Nombre de representante: " << nombreR << "\n- Numero de telefono: " << telefono << "\n- Email: "
         << "\n- Direccion: "
         << "\n\nIngrese un email: ";
    getline(cin >> ws, email);
    system(clear);
    cout << "\n\nIngresar un correo postal\n"
         << "\n- Nombre de la empresa: " << nombreEmpresa
         << "\n- Numero de celular: " << celular << "\n- Nombre de representante: " << nombreR << "\n- Numero de telefono: " << telefono << "\n- Email: " << email << "\n- Direccion: "
         << "\n\nIngrese una direccion: ";
    getline(cin >> ws, direccion);
    system(clear);
    cout << "\n\nIngresar un Cliente\n"
         << "\n- Numero de celular: " << celular << "\n- Nombre de representante: " << nombreR << "\n- Numero de telefono: " << telefono << "\n- Email: " << email << "\n- Direccion: " << direccion << "\n\nDesea agregar al correo postal (y/n) ? ";
    cin >> confirmar;
    if (confirmar == 'y')
    {
        cout << "\n Ha agregado correctamente el correo postal";
        cout << "\n Presione enter para continuar";
        cin.clear();
        cin.ignore();
        cin.get();
        return Correo(nombreEmpresa,celular, nombreR, telefono, email, direccion);
    }
}

void listarClientes(vector<Cliente> clientes)
{
    cout << "Lista de clientes \n\n";
    {
        for (int i = 0; i < static_cast<int>(clientes.size()); i++)
        {
            cout << "\n[" << i << "] "
                 << "Nombre: " << clientes[i].getNombre() << "\nRut: " << clientes[i].getRut() << "\nCliente Id: " << clientes[i].getClienteId() << "\n"
                 << endl;
        }
    }
}

void listarCorreos(vector<Correo> correosPostales)
{
    cout << "Lista de correos postales \n\n";
    {
        for (int i = 0; i < static_cast<int>(correosPostales.size()); i++)
        {
            cout << "\n[" << i << "]" << " Nombre de la empresa: " << correosPostales[i].getNombreEmpresa()
                 << "\nCelular: " << correosPostales[i].getCelular() << "\nNombre de representante: " << correosPostales[i].getRepresentante() << "\nTelefono: " << correosPostales[i].getTelefono() << "\nEmail: " << correosPostales[i].getEmail() << "\nDireccion: " << correosPostales[i].getDireccion() << endl;
        }
    }
}

void pause()
{
    cout << "\n Presione enter para continuar...";
    cin.clear();
    cin.ignore();
    cin.get();
}

int menu()
{
    int opcion;
    system(clear);
    cout << "\n\nMenu de Opciones" << endl;
    cout << "1. Ingresar un cliente" << endl;
    cout << "2. Ingresar un producto" << endl;
    cout << "3. Ingresar un pedido" << endl;
    cout << "4. Ingresar una critica a un pedido" << endl;
    cout << "5. Ingresar empresa de correo" << endl;
    cout << "6. Listar pedidos no entregados" << endl;
    cout << "7. Listar pedidos entregados" << endl;
    cout << "8. Listar todos los clientes" << endl;
    cout << "9. Listar todos los pedidos" << endl;
    cout << "10. Listar empresas de correo" << endl;
    cout << "11. Listar criticas de un pedido" << endl;
    cout << "0. SALIR" << endl;
    cout << "\nIngrese una opcion: ";
    cin >> opcion;
    return opcion;
}

#endif // MENU_H
