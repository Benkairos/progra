#ifndef MENUP_H
#define MENUP_H

#include "module.h"
#include "menu.h"
#include "string"
#include <iostream>
#include <vector>

void listarPedidoEntregado(vector<Pedido> pedido)
{
    cout << "***************************" << endl;
    cout << "Lista de Pedidos Entregados\n";
    cout << "*************************** \n"
         << endl;
    {
        for (int i = 0; i < static_cast<int>(pedido.size()); i++)
        {
            if (pedido[i].getEstadoPedido() == 4)
            {
                cout << "\n[" << i << "] "
                     << "Nombre de cliente: " << pedido[i].getCliente().getNombre() << "\nEmpresa de correo postal: " << pedido[i].getCorreoPostal().getRepresentante() << "\nEstado del pedido: "
                     << "Entregado"
                     << "\nLista de Productos:" << endl;
                for (int j = 0; j < static_cast<int>(pedido[i].getPedidoDetalle().size()); j++)
                {
                    cout << "\nNombre del producto: " << pedido[i].getPedidoDetalle()[j].getProducto().getNombreProducto()
                         << "\nCantidad del producto: " << pedido[i].getPedidoDetalle()[j].getCantidad()
                         << "\nPrecio: " << pedido[i].getPedidoDetalle()[j].getTotal() << "\n"
                         << endl;
                    cout << "**********************************************************+**" << endl;
                }
            }
        }
    }
}

void listarPedidoNentregado(vector<Pedido> pedido)
{
    cout << "***************************" << endl;
    cout << "Lista de Pedidos No Entregados\n";
    cout << "*************************** \n"
         << endl;
    {
        for (int i = 0; i < static_cast<int>(pedido.size()); i++)
        {
            if (pedido[i].getEstadoPedido() != 4)
            {
                string estado;
                switch (pedido[i].getEstadoPedido())
                {
                case 0:
                    estado = "pendiente";
                    break;
                case 1:
                    estado = "pagado";
                    break;
                case 3:
                    estado = "enviado";
                    break;
                case 4:
                    estado = "entregado";
                    break;
                default:
                    break;
                }
                cout << "\n[" << i << "] "
                     << "Nombre de cliente: " << pedido[i].getCliente().getNombre() << "\nEmpresa de correo postal: " << pedido[i].getCorreoPostal().getRepresentante() << "\nEstado del pedido: " << estado << "\nLista de Productos:" << endl;
                for (int j = 0; j < static_cast<int>(pedido[i].getPedidoDetalle().size()); j++)
                {
                    cout << "\nNombre del producto: " << pedido[i].getPedidoDetalle()[j].getProducto().getNombreProducto()
                         << "\nCantidad del producto: " << pedido[i].getPedidoDetalle()[j].getCantidad()
                         << "\nPrecio: " << pedido[i].getPedidoDetalle()[j].getTotal() << "\n"
                         << endl;
                    cout << "**********************************************************+**" << endl;
                }
            }
        }
    }
}

void listarTodosLosPedidos(vector<Pedido> pedido)
{
    cout << "***************************" << endl;
    cout << "Lista de Pedidos No Entregados\n";
    cout << "*************************** \n"
         << endl;
    {
        for (int i = 0; i < static_cast<int>(pedido.size()); i++)
        {
            string estado;
            switch (pedido[i].getEstadoPedido())
            {
            case 0:
                estado = "pendiente";
                break;
            case 1:
                estado = "pagado";
                break;
            case 3:
                estado = "enviado";
                break;
            case 4:
                estado = "entregado";
                break;
            default:
                break;
            }
            cout << "\n[" << i << "] "
                 << "Nombre de cliente: " << pedido[i].getCliente().getNombre() << "\nEmpresa de correo postal: " << pedido[i].getCorreoPostal().getRepresentante() << "\nEstado del pedido: " << estado << "\nLista de Productos:" << endl;
            for (int j = 0; j < static_cast<int>(pedido[i].getPedidoDetalle().size()); j++)
            {
                cout << "\nNombre del producto: " << pedido[i].getPedidoDetalle()[j].getProducto().getNombreProducto()
                     << "\nCantidad del producto: " << pedido[i].getPedidoDetalle()[j].getCantidad()
                     << "\nPrecio: " << pedido[i].getPedidoDetalle()[j].getTotal() << "\n"
                     << endl;
                cout << "**********************************************************+**" << endl;
            }
        }
    }
}

bool deseaContinuar(string msj)
{
    char selector;
    cout << msj;
    cin >> selector;
    while (true)
    {
        if (selector == 'y' || selector == 'Y')
        {
            return true;
        }
        else if (selector == 'n' || selector == 'N')
        {
            return false;
        }

        cout << "ERROR: " << msj;
        cin.clear();
        cin.ignore();
        cin >> selector;
    }
}

int retornador(int leng)
{
    int selector;
    cin >> selector;
    while (true)
    {
        if (selector >= leng || selector < 0)
        {
            cout << "ERROR: Porfavor seleccione una opción correcta: ";
            cin.clear();
            cin >> selector;
        }
        else
        {
            return selector;
        }
    }
}

void listarProductos(vector<Productos> productos)
{
    cout << "Listado de Productos \n\n";
    {
        for (int i = 0; i < static_cast<int>(productos.size()); i++)
        {
            cout << "\n[" << i << "] "
                 << "Nombre de producto: " << productos[i].getNombreProducto() << "\nPrecio sin iva " << productos[i].getPrecio() << "\nPrecio con iva : " << productos[i].getImpuesto() << endl;
        }
    }
}

float calcularPrecioTotal(vector<ProductoDetalle> p)
{
    float total = 0;
    for (int i = 0; i < static_cast<int>(p.size()); i++)
    {
        total = p[i].getTotal() + total;
    }
    return total;
}

void listarProductosSeleccionados(vector<ProductoDetalle> p)
{
    cout << "\n\t\t"
         << " (Nombre del Producto) "
         << "\t"
         << "(SubTotal) \t"
         << "(Cantidad)" << endl;
    for (int i = 0; i < static_cast<int>(p.size()); i++)
    {
        cout << "\n \t \t - " << p[i].getProducto().getNombreProducto() << "\t \t $" << p[i].getTotal() << " \t \t" << p[i].getCantidad() << " unidades " << endl;
    }
}

Pedido menuPedido(vector<Cliente> clientes, vector<Correo> correosP, vector<Productos> produc)
{
    int selector;
    Cliente cliente;
    Correo correo;
    Productos producto;
    int cantidad;
    float precioTotal;
    int dia;
    int mes;
    int anio;
    float total;
    vector<ProductoDetalle> selProduc;
    // Menu Clientes
    system(clear);
    cin.clear();
    cout << "\n\nMenu de Pedidos"
         << "\n \n";
    listarClientes(clientes);
    cout << "\nEscoja un cliente: ";
    selector = retornador(static_cast<int>(clientes.size()));
    cliente = clientes[selector];
    // Menu correo postal

    system(clear);
    cin.clear();
    cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n \n";
    listarCorreos(correosP);
    cout << "\nEscoja un Correo Postal: ";
    selector = retornador(static_cast<int>(correosP.size()));
    correo = correosP[selector];

    // Elegir productos
    while (true)
    {
        system(clear);
        cin.clear();
        bool flag = false;
        cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n";
        listarProductos(produc);
        cout << "\nEscoja un numero de producto: ";
        selector = retornador(static_cast<int>(produc.size()));
        producto = produc[selector];
        cout << "Cantidad: ";
        cin >> cantidad;
        precioTotal = producto.getImpuesto() * cantidad;
        for (int i = 0; i < static_cast<int>(selProduc.size()); i++)
        {
            if (selProduc[i].getProducto().getNombreProducto() == producto.getNombreProducto())
            {
                selProduc[i].setTotal(selProduc[i].getTotal() + precioTotal);
                selProduc[i].setCantidad(selProduc[i].getCantidad() + cantidad);
                flag = true;
            }
        }
        if (!flag)
        {
            selProduc.push_back(ProductoDetalle(producto, cantidad, precioTotal));
        }
        total = calcularPrecioTotal(selProduc);
        listarProductosSeleccionados(selProduc);
        cout << "\n \t \tSu pago total es de : " << total << "\n";
        if (!(deseaContinuar("Desea seguir agregando productos (y/n) ? ")))
            break;
    }

    // Menu de fecha
    system(clear);
    cin.clear();
    cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n \n";
    cout << "\n Ingrese fecha en que se formaliza o se formalizó el pedido:  \n\n"
         << "\t\t Ejemplo de fecha: 01/12/2020 (DD/MM/YYYY) \n\n"
         << "\t\t\t\t Ingrese dia:  ";
    cin >> dia;
    system(clear);
    cin.clear();
    cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n \n";
    cout << "\n Ingrese fecha en que se formaliza o se formalizó el pedido:  \n\n"
         << "\t\t Ejemplo de fecha: 01/12/2020 (DD/MM/YYYY) \n\n"
         << "\t\t\t\t Ingrese mes:  " << dia << "/";
    cin >> mes;
    system(clear);
    cin.clear();
    cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n \n";
    cout << "\n Ingrese fecha en que se formaliza o se formalizó el pedido:  \n\n"
         << "\t\t Ejemplo de fecha: 01/12/2020 (DD/MM/YYYY) \n\n"
         << "\t\t\t\t Ingrese año:  " << dia << "/" << mes << "/";
    cin >> anio;

    // Menu de estado
    system(clear);
    cin.clear();
    cout << "\n\nAñadiendo un pedido para " << cliente.getNombre() << "\n \n";
    cout << "\n Ingrese el estado en que se encuentra el pedido:  \n\n"
         << "\t\t [1] pendiente \n"
         << "\t\t [2] pagado \n"
         << "\t\t [3] enviado \n"
         << "\t\t [4] entregado \n"
         << "\t\t\t\t Estado Actual (procesando) \n"
         << "\t\t Seleccione el estado:  ";
    selector = retornador(5);
    EstadoPedido estado;
    switch (selector)
    {
    case 1:
        estado = pendiente;
        break;
    case 2:
        estado = pagado;
        break;
    case 3:
        estado = enviado;
        break;
    case 4:
        estado = entregado;
        break;
    default:
        break;
    }

    time_t pedidoId;
    pedidoId = time(NULL);
    string fecha = to_string(dia) + "/" + to_string(mes) + "/" + to_string(anio);
    return Pedido(cliente, estado, correo, to_string(pedidoId), fecha, selProduc, total);
}

#endif // MENUP_H